package ir.mk.notepad.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import ir.mk.notepad.entities.NoteEntity

@Database(
    entities = [NoteEntity::class],
    version = DBHandler.DATABASE_VERSION
)
abstract class DBHandler : RoomDatabase() {
    abstract fun noteDao(): NoteDao

    companion object {

        private const val DATABASE_NAME = "notepad_database"
        const val DATABASE_VERSION = 1

        const val NOTE_TABLE = "noteTable"

        private var INSTANCE: DBHandler? = null

        fun getDatabase(context: Context): DBHandler {

            if (INSTANCE == null)
                INSTANCE = Room.databaseBuilder(
                    context,
                    DBHandler::class.java,
                    DATABASE_NAME
                )
                    .fallbackToDestructiveMigration()
                    .build()

            return INSTANCE!!
        }
    }
}