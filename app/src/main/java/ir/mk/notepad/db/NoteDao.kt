package ir.mk.notepad.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import ir.mk.notepad.entities.NoteEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface NoteDao {
    @Insert
    fun insertNote( note: NoteEntity):Long

    @Update
    fun updateNote(note: NoteEntity): Int

    @Query("SELECT * FROM ${DBHandler.NOTE_TABLE}")
    fun getNotes(): Flow<List<NoteEntity>>

    @Query("SELECT * FROM ${DBHandler.NOTE_TABLE} WHERE text LIKE '%' || :string || '%' OR title LIKE '%' || :string || '%'")
    fun getNotes(string:String): Flow<List<NoteEntity>>

    @Query("SELECT * FROM ${DBHandler.NOTE_TABLE} where id =:id")
    fun getNote(id:Int): Flow<NoteEntity?>

    @Delete
    fun remove(vararg note: NoteEntity): Int

    @Query("SELECT * FROM ${DBHandler.NOTE_TABLE} ORDER BY id DESC LIMIT 1")
    fun getLastInsertedNote(): Flow<NoteEntity?>
}