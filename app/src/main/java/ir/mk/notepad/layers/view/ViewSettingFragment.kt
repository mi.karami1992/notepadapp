package ir.mk.notepad.layers.view

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatDelegate
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import ir.mk.notepad.adapter.NoteAdapter
import ir.mk.notepad.databinding.FragmentSettingBinding
import ir.mk.notepad.entities.NoteEntity
import ir.mk.notepad.layers.ext.ActivityUtils
import ir.mk.notepad.layers.ext.MyContextWrapper
import ir.mk.notepad.layers.ext.OnNoteBindData
import ir.mk.notepad.layers.ui.NavDrawerActivity
import ir.mk.notepad.layers.ui.fragments.NoteFragment

class ViewSettingFragment(private var activity: Activity, private var activityUtil:ActivityUtils) : FrameLayout(activity) {
    val binding = FragmentSettingBinding.inflate(LayoutInflater.from(activity))
    fun handleDark(){
        binding.darkSwitch.setOnCheckedChangeListener { _, b ->
            MyContextWrapper.wrap(context, "fa")
            resources.updateConfiguration(resources.configuration, resources.displayMetrics)
        }

    }
    fun toggleTheme(isDarkMode: Boolean) {
        val nightMode = if (isDarkMode) {
            AppCompatDelegate.MODE_NIGHT_YES
        } else {
            AppCompatDelegate.MODE_NIGHT_NO
        }
        AppCompatDelegate.setDefaultNightMode(nightMode)
    }

}