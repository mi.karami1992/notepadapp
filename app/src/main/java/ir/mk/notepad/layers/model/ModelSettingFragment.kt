package ir.mk.notepad.layers.model

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import ir.mk.notepad.db.DBHandler
import ir.mk.notepad.entities.NoteEntity
import ir.mk.notepad.layers.ext.OnNoteBindData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ModelSettingFragment(private val activity: AppCompatActivity) {
    private val db = DBHandler.getDatabase(activity)
}