package ir.mk.notepad.layers.ext

import ir.mk.notepad.entities.NoteEntity
import ir.mk.notepad.entities.Task

interface OnTaskBindData {
    fun deleteTask(task: Task,position: Int) {}
    fun updateSentence(task:Task, position: Int) {}
    fun updateCheckMode(task:Task, position: Int) {}

}