package ir.mk.notepad.layers.view

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatDelegate
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import ir.mk.notepad.adapter.NoteAdapter
import ir.mk.notepad.databinding.FragmentHomeBinding
import ir.mk.notepad.entities.NoteEntity
import ir.mk.notepad.layers.ext.ActivityUtils
import ir.mk.notepad.layers.ext.OnNoteBindData
import ir.mk.notepad.layers.ui.NavDrawerActivity
import ir.mk.notepad.layers.ui.fragments.NoteFragment

class ViewHomeFragment(private var activity: Activity,private var activityUtil:ActivityUtils) : FrameLayout(activity) {

    val binding = FragmentHomeBinding.inflate(LayoutInflater.from(activity))
    private lateinit var adapter: NoteAdapter
    fun initRecycler(notes: ArrayList<NoteEntity>, onNoteBindData: OnNoteBindData) {
        val layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        binding.noteList.layoutManager = layoutManager
        adapter = NoteAdapter(activity, notes, onNoteBindData,activityUtil)
        binding.noteList.adapter = adapter
    }

    fun updateNotes(notes: List<NoteEntity>){
        adapter.dataUpdate(ArrayList(notes))
    }

    fun initAddFloatingActionButton() {
        binding.fabAddNote.setOnClickListener {
//            val intent = Intent(activity, AddNoteActivity::class.java)
//            intent.putExtra("addNew", "true")
//            activity.startActivity(intent)
            activityUtil.setFragment(NoteFragment(0,activityUtil))
        }

    }

    fun showNotes(notes: List<NoteEntity>) {
        val data = arrayListOf<NoteEntity>()
        notes.forEach { data.add(it) }
        adapter.dataUpdate(data)
    }

    fun loadComponents(onNoteBindData: OnNoteBindData) {
        binding.customAppBar.showNavDrawer(activity)
        binding.customAppBar.initSearchTextListoner(activity, object : OnNoteBindData {
            override fun search(str: String) {
                onNoteBindData.search(str)
            }
        })
    }

}