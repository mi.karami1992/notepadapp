package ir.mk.notepad.layers.ui.customView

import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import ir.mk.notepad.R
import ir.mk.notepad.databinding.CustomAppBarBinding
import ir.mk.notepad.layers.ext.OnNoteBindData
import ir.mk.notepad.layers.ui.NavDrawerActivity


class CustomAppBar(
    context: Context,
    attrs: AttributeSet
) : FrameLayout(context, attrs) {

    private val binding =
        CustomAppBarBinding.inflate(LayoutInflater.from(context))

    init {

        addView(binding.root)

        initialize(attrs)

    }

    private fun initialize(attrs: AttributeSet) {

        val typeArray = context.obtainStyledAttributes(attrs, R.styleable.CustomAppBar)

        val isBack = typeArray.getBoolean(R.styleable.CustomAppBar_backIcon, false)

        if (isBack) {

        }

        typeArray.recycle()

    }


    fun countShow() {
    }

    fun countHide() {
    }

    fun setCount(count: String) {
    }

    fun showNavDrawer(context: Context) {

        binding.imgMenu.setOnClickListener {
            context.startActivity(Intent(context, NavDrawerActivity::class.java))
        }

    }

    fun initSearchTextListoner(context: Context, onNoteBindData: OnNoteBindData) {
        binding.edtSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(string: Editable?) {
                    onNoteBindData.search(string.toString())
            }


        })
    }

}