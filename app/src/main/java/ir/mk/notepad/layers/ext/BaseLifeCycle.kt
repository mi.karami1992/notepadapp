package ir.mk.notepad.layers.ext

interface BaseLifeCycle {
    fun onCreate()
    fun onDestroy(){}
    fun onStop(){}
}