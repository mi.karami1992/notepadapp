package ir.mk.notepad.layers.presenter

import ir.mk.notepad.entities.NoteEntity
import ir.mk.notepad.layers.ext.BaseLifeCycle
import ir.mk.notepad.layers.ext.OnNoteBindData
import ir.mk.notepad.layers.model.ModelHomeFragment
import ir.mk.notepad.layers.view.ViewHomeFragment

class PresenterHomeFragment(
    private val view: ViewHomeFragment,
    private val model: ModelHomeFragment
) : BaseLifeCycle {
    override fun onCreate() {
        view.loadComponents(object:OnNoteBindData{
            override fun search(str: String) {
                if(str.length>2) {
                    model.getNotes(object : OnNoteBindData {
                        override fun getData(noteEnties: List<NoteEntity>) {
                            view.updateNotes(noteEnties)
                        }
                    }, str)
                }else{
                    model.getNotes(object : OnNoteBindData {
                        override fun getData(noteEnties: List<NoteEntity>) {
                            view.updateNotes(noteEnties)
                        }
                    })
                }
            }
        })
        initNoteRecycler()
        dataHandle()
        initAddNote()
    }

    private fun initAddNote() {
        view.initAddFloatingActionButton()
    }

    private fun initNoteRecycler() {
        view.initRecycler(arrayListOf(),object :OnNoteBindData{
            override fun deleteData(noteEntity: NoteEntity) {
                model.removeNote(noteEntity)
            }
        })
    }

    private fun dataHandle(){
        model.getNotes(object:OnNoteBindData{
            override fun getData(notes: List<NoteEntity>) {
                view.showNotes(notes)
            }
        })
    }

//    private fun initData(){
//        view.setText(model.getData())
//    }

    override fun onDestroy() {
        super.onDestroy()
    }

}