package ir.mk.notepad.layers.model

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import ir.mk.notepad.db.DBHandler
import ir.mk.notepad.entities.NoteEntity
import ir.mk.notepad.layers.ext.OnNoteBindData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ModelHomeFragment(private val activity: AppCompatActivity) {
    private val db = DBHandler.getDatabase(activity)
    fun getNotes(onNoteBindData: OnNoteBindData){
        activity.lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                val tasks = db.noteDao().getNotes()
                withContext(Dispatchers.Main) {
                    tasks.collect {
                        onNoteBindData.getData(it)
                    }
                }
            }
        }
    }    fun getNotes(onNoteBindData: OnNoteBindData,string:String){
        activity.lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                val tasks = db.noteDao().getNotes(string)
                withContext(Dispatchers.Main) {
                    tasks.collect {
                        onNoteBindData.getData(it)
                    }
                }
            }
        }
    }
    
    fun removeNote(item:NoteEntity){
        activity.lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                db.noteDao().remove(item)
            }
        }
    }
}