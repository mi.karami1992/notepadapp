package ir.mk.notepad.layers.ext

import ir.mk.notepad.entities.NoteEntity

interface OnNoteBindData {
    fun deleteData(note: NoteEntity) {}
    fun saveNote(note: NoteEntity) {}
    fun getData(noteEntity: List<NoteEntity>) {}
    fun showData(noteEntity: NoteEntity){}
    fun getLast(noteEntity: NoteEntity) {}
    fun updateData(note: NoteEntity) {}
    fun deleteTask(position: Int) {}
    fun getNoteById(id: Int){}
     fun returnSavedNoteEntity(noteEntity: NoteEntity){}
    fun search(str:String){}
}