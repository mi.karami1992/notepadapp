package ir.mk.notepad.layers.ui

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import ir.mk.notepad.R
import ir.mk.notepad.databinding.ActivityMainBinding
import ir.mk.notepad.layers.ext.ActivityUtils
import ir.mk.notepad.layers.ui.fragments.HomeFragment
import ir.mk.notepad.layers.ui.fragments.NoteFragment

class MainActivity : AppCompatActivity(), ActivityUtils {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(LayoutInflater.from(this))
        when (intent.getStringExtra("menuItem")) {
            null ->setFragment(HomeFragment(this))
            "home" -> setFragment(HomeFragment(this))
            else -> { // Note the block
                finish()
            }
        }

        setContentView(binding.root)
    }

    override fun setFragment(fragment: Fragment) {

        supportFragmentManager.beginTransaction()
            .replace(R.id.mainFragmentContainerView, fragment)
            .commit()

    }

    override fun onBackPressed() {
        if (!handleBack())
            super.onBackPressed()

    }

    private fun handleBack(): Boolean {
        val fragment = supportFragmentManager.findFragmentById(R.id.mainFragmentContainerView)
        if (fragment is NoteFragment) {
            setFragment(HomeFragment( this))
            return true
        }
        return false


    }
}