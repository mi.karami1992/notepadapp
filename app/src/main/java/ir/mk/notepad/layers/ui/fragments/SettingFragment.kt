import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import ir.mk.notepad.layers.ext.ActivityUtils
import ir.mk.notepad.layers.model.ModelSettingFragment
import ir.mk.notepad.layers.presenter.PresenterSettingFragment
import ir.mk.notepad.layers.view.ViewSettingFragment


class SettingFragment( var activityUtils: ActivityUtils) : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var presenter: PresenterSettingFragment

        val view = ViewSettingFragment(requireActivity(),activityUtils)
        val model = ModelSettingFragment(activity as AppCompatActivity )
        presenter=PresenterSettingFragment(view,model)
        presenter.onCreate()
        // Inflate the layout for this fragment
        return view.binding.root
    }
}