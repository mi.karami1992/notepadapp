package ir.mk.notepad.layers.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.GestureDetector
import android.view.LayoutInflater
import android.view.MotionEvent
import ir.mk.notepad.R
import ir.mk.notepad.databinding.ActivityMainBinding
import ir.mk.notepad.databinding.ActivityNavDrawerBinding
import ir.mk.notepad.databinding.NoteItemBinding

class NavDrawerActivity : AppCompatActivity(), GestureDetector.OnGestureListener {
    private lateinit var gestureDetector: GestureDetector
    private lateinit var binding:ActivityNavDrawerBinding
    private lateinit var intent:Intent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         binding = ActivityNavDrawerBinding.inflate(LayoutInflater.from(this))

        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out)
        gestureDetector = GestureDetector(this, this)
        intent = Intent(this, MainActivity::class.java)

        binding.txtItem0.setOnClickListener {
            selectMenuItem("home")
        }
        binding.txtItem1.setOnClickListener {
            selectMenuItem("setting")
        }

        setContentView(binding.root)

    }
    private fun selectMenuItem(menuItem:String){
        intent.putExtra("menuItem", menuItem)
        startActivity(intent)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        // به GestureDetector اجازه می‌دهد که رویدادهای لمسی را بررسی کند
        gestureDetector.onTouchEvent(event!!)
        return super.onTouchEvent(event)
    }

    override fun onDown(p0: MotionEvent): Boolean = true

    override fun onShowPress(p0: MotionEvent) {
    }

    override fun onSingleTapUp(p0: MotionEvent): Boolean = true

    override fun onScroll(p0: MotionEvent?, p1: MotionEvent, p2: Float, p3: Float): Boolean = true

    override fun onLongPress(p0: MotionEvent) {
    }

    override fun onFling(e1: MotionEvent?, e2: MotionEvent, p2: Float, p3: Float): Boolean {
        if (e2 != null && e1 != null) {
            if (e1.x - e2.x > 100) { // بررسی حرکت به چپgha
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out)
                finish()
                return true
            }
        }
        return false
    }

}