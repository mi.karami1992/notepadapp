package ir.mk.notepad.layers.model

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import ir.mk.notepad.db.DBHandler
import ir.mk.notepad.entities.NoteEntity
import ir.mk.notepad.layers.ext.OnNoteBindData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.Date

class ModelNoteFragment(private val activity: AppCompatActivity) {


    private val db = DBHandler.getDatabase(activity)

    fun addNote(noteEntity: NoteEntity,onNoteBindData: OnNoteBindData) {
        activity.lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                noteEntity.updatedAt=System.currentTimeMillis()
                noteEntity.createdAt=System.currentTimeMillis()
                var noteId:Long = db.noteDao().insertNote(noteEntity)
                noteEntity.id=noteId.toInt()
                onNoteBindData.returnSavedNoteEntity(noteEntity)
            }
        }
    }
    fun removeNote(item:NoteEntity){
        activity.lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                db.noteDao().remove(item)
            }
        }
    }

//    fun getLastNode(onNoteBindData: OnNoteBindData) {
//        activity.lifecycleScope.launch {
//            withContext(Dispatchers.IO) {
//                val task = db.noteDao().getLastInsertedNote()
//                withContext(Dispatchers.Main) {
//                    task.collect {
//                        onNoteBindData.getLast(it!!)
//                    }
//                }
//            }
//        }
//    }

    fun updateNote(noteEntity: NoteEntity) {
        activity.lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                noteEntity.updatedAt=System.currentTimeMillis()
                db.noteDao().updateNote(noteEntity)
            }
        }
    }
    fun saveNote(noteEntity: NoteEntity) {
        activity.lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                db.noteDao().insertNote(noteEntity)
            }
        }
    }

    fun getNoteById(onNoteBindData: OnNoteBindData, noteId: Int) {
        activity.lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                val note = db.noteDao().getNote(noteId)
                withContext(Dispatchers.Main) {
//                    note.collect {
                        onNoteBindData.showData(note.first()!!)
//                    }
                }

            }
        }
    }


}