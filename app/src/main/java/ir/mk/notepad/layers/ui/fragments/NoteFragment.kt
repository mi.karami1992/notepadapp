package ir.mk.notepad.layers.ui.fragments

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import ir.mk.notepad.layers.ext.ActivityUtils
import ir.mk.notepad.layers.model.ModelNoteFragment
import ir.mk.notepad.layers.presenter.PresenterNoteFragment
import ir.mk.notepad.layers.view.ViewNoteFragment


class NoteFragment(var noteId:Int,var activityUtils: ActivityUtils) : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var presenter: PresenterNoteFragment
        val view = ViewNoteFragment(requireActivity(),activityUtils,noteId)
        val model = ModelNoteFragment(activity as AppCompatActivity )
        presenter=PresenterNoteFragment(view,model)
        presenter.onCreate()
        return view.binding.root
    }

}