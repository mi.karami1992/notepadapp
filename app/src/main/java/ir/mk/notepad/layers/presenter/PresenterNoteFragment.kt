package ir.mk.notepad.layers.presenter

import ir.mk.notepad.entities.NoteEntity
import ir.mk.notepad.layers.ext.BaseLifeCycle
import ir.mk.notepad.layers.ext.OnNoteBindData
import ir.mk.notepad.layers.model.ModelNoteFragment
import ir.mk.notepad.layers.view.ViewNoteFragment

class PresenterNoteFragment(
    private val view: ViewNoteFragment,
    private val model: ModelNoteFragment
) : BaseLifeCycle {
    override fun onCreate() {
        view.init(object : OnNoteBindData {
            override fun updateData(note: NoteEntity) {
                model.updateNote(note)
            }
            override fun saveNote(note: NoteEntity) {
                model.addNote(note, object:OnNoteBindData{
                    override fun returnSavedNoteEntity(noteEntity: NoteEntity) {
                        view.setSavedNoteEntity(noteEntity)
                    }
                })
            }
            override fun deleteData(noteEntity: NoteEntity) {
                model.removeNote(noteEntity)
            }
            override fun getNoteById(id: Int) {
                model.getNoteById(object : OnNoteBindData {
                    override fun showData(noteEntity: NoteEntity) {
                        view.showNote(noteEntity,object :OnNoteBindData{
                            override fun updateData(note: NoteEntity) {
                                model.updateNote(note)
                            }
                        })
                    }
                }, id);
            }
        })
        view.initTextListener(object :OnNoteBindData{
            override fun updateData(note: NoteEntity) {
                model.updateNote(note)
            }
        })
        view.handleViews(object :OnNoteBindData{
            override fun updateData(note: NoteEntity) {
                model.updateNote(note)
            }
        })
    }
}