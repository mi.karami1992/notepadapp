package ir.mk.notepad.layers.ui.fragments

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import ir.mk.notepad.layers.ext.ActivityUtils
import ir.mk.notepad.layers.model.ModelHomeFragment
import ir.mk.notepad.layers.presenter.PresenterHomeFragment
import ir.mk.notepad.layers.view.ViewHomeFragment


class HomeFragment(var activityUtils: ActivityUtils) : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

         var presenter: PresenterHomeFragment

        val view = ViewHomeFragment(requireActivity(),activityUtils)
        val model = ModelHomeFragment(requireActivity() as AppCompatActivity )
        presenter=PresenterHomeFragment(view,model)
        presenter.onCreate()
        // Inflate the layout for this fragment
        return view.binding.root
    }

}