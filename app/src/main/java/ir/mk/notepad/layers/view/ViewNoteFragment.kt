package ir.mk.notepad.layers.view

import TaskAdapter
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ir.mk.notepad.R
import ir.mk.notepad.databinding.FragmentNoteBinding
import ir.mk.notepad.entities.NoteEntity
import ir.mk.notepad.entities.Task
import ir.mk.notepad.layers.ext.ActivityUtils
import ir.mk.notepad.layers.ext.OnNoteBindData
import ir.mk.notepad.layers.ext.OnTaskBindData
import ir.mk.notepad.layers.ext.toDate
import ir.mk.notepad.layers.ui.fragments.HomeFragment
import java.util.Date


class ViewNoteFragment(private val contextInstance: Activity, private var activityUtil: ActivityUtils, var noteId: Int) :
    FrameLayout(contextInstance) {
    val binding = FragmentNoteBinding.inflate(LayoutInflater.from(context))
    lateinit var noteEntity: NoteEntity
    lateinit var adapter: TaskAdapter
    var taskList: ArrayList<Task> = ArrayList()
    private var isOpenTopCardView = false


    fun init(onNoteBindData: OnNoteBindData) {

        if (noteId == 0) {
            createNewNote(onNoteBindData)
        } else {
            onNoteBindData.getNoteById(noteId)
        }
        handleBottomCommands(onNoteBindData)
    }

    fun initTextListener(onNoteBindData: OnNoteBindData) {
        binding.text.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(string: Editable?) {
                if (string.toString() != noteEntity.text) {
                    noteEntity.text = string.toString()
                    onNoteBindData.updateData(noteEntity)
                }
            }
        })
        binding.title.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(string: Editable?) {
                if (string.toString() != noteEntity.title) {
                    noteEntity.title = string.toString()
                    onNoteBindData.updateData(noteEntity)
                }
            }
        })
        binding.addNewItem.setOnClickListener {
            noteEntity.text += "\n"
            noteEntity.checkString += "0"
            onNoteBindData.updateData(noteEntity)
            val task = Task("", false);
            taskList.add(task)
            adapter.addnew(task)
        }
    }

    fun showTxtMode() {
        binding.tasksRecyclerView.visibility = View.GONE
        binding.addNewItem.visibility = View.GONE
        binding.text.visibility = View.VISIBLE
        binding.text.setText(noteEntity.text.toString())

    }

    private fun createNewNote(onNoteBindData: OnNoteBindData) {
        noteEntity =
            NoteEntity(0, "", "", "", false, System.currentTimeMillis(), System.currentTimeMillis())
        onNoteBindData.saveNote(noteEntity)
    }

    private fun splitText(text: String): List<String>? {
        var list = text?.split("\n")
        if (list?.last() == "")
            list = list?.dropLast(1)
        return list

    }

    private fun showCheckMode(
        noteEntity: NoteEntity,
        list: List<String>,
        onNoteBindData: OnNoteBindData
    ) {
        binding.tasksRecyclerView.visibility = View.VISIBLE
        binding.addNewItem.visibility = View.VISIBLE
        binding.text.visibility = View.GONE
        initRecycler(noteEntity, list, onNoteBindData)
    }


    private fun initRecycler(
        noteEntity: NoteEntity,
        list: List<String>,
        onNoteBindData: OnNoteBindData
    ) {
        taskList.clear()
        for (i in 0 until noteEntity.checkString.length) {
            if (noteEntity.checkString[i] == '0') {
                taskList.add(Task(list.get(i), false))
            } else {
                taskList.add(Task(list.get(i), true))
            }
        }


        adapter = TaskAdapter(taskList, object : OnTaskBindData {
            override fun updateSentence(task: Task, position: Int) {
                taskList.get(position).sentence = task.sentence
                var newString = ""
                taskList.forEach { newString += it.sentence + "\n" }
                newString = newString.substring(0, newString.length - 1)
                noteEntity.text = newString
                onNoteBindData.updateData(noteEntity)
            }

            override fun updateCheckMode(task: Task, position: Int) {
                taskList.get(position).isChecked = task.isChecked
                var newCheckString = "";
                taskList.forEach {
                    if (it.isChecked)
                        newCheckString += "1"
                    else
                        newCheckString += "0"

                }
                noteEntity.checkString = newCheckString
                onNoteBindData.updateData(noteEntity)
            }

            override fun deleteTask(task: Task, position: Int) {
                taskList.removeAt(position)
                var newString = ""
                taskList.forEach { newString += it.sentence + "\n" }
                if (taskList.size > 0)
                    newString = newString.substring(0, newString.length - 1)
                noteEntity.text = newString
                var chStr = noteEntity.checkString.removeRange(position, position + 1)
                noteEntity.checkString = chStr
                onNoteBindData.updateData(noteEntity)
            }
        })
        binding.tasksRecyclerView.layoutManager =
            LinearLayoutManager(contextInstance, RecyclerView.VERTICAL, false)
        binding.tasksRecyclerView.adapter = adapter
    }

    fun showNote(noteEntity: NoteEntity, onNoteBindData: OnNoteBindData) {
        this.noteEntity = noteEntity
        binding.updateTime.text = (noteEntity.updatedAt.toDate())
        binding.title.setText(noteEntity.title.toString())
        if (noteEntity.checkMode) {
            var list = splitText(noteEntity.text)!!
            showCheckMode(noteEntity, list, onNoteBindData)
        } else {
            showTxtMode()
        }
        binding.updateTime.text = noteEntity.updatedAt.toDate()
        binding.addCheckBox.isChecked = noteEntity.checkMode

    }

    fun setSavedNoteEntity(noteEntity: NoteEntity) {
        binding.updateTime.text = noteEntity.updatedAt.toDate()
        this.noteEntity = noteEntity
    }

    fun handleViews(onNoteBindData: OnNoteBindData) {

        binding.icPlus.setOnClickListener {
            if (binding.topCardView.visibility == View.VISIBLE) {
                if (binding.deleteNote.visibility == View.VISIBLE) {
                    showCheckBox()
                    hideMore()
                } else {
                    binding.topCardView.visibility = View.GONE
                }
            } else {
                binding.topCardView.visibility = View.VISIBLE
                showCheckBox()
                hideMore()
            }

        }

        binding.icMore.setOnClickListener {
            if (binding.topCardView.visibility == View.VISIBLE) {
                if (binding.addCheckBox.visibility == VISIBLE) {
                    hideCheckBox()
                    showMore()
                } else {
                    binding.topCardView.visibility = View.GONE
                }

            } else {
                binding.topCardView.visibility = View.VISIBLE
                hideCheckBox()
                showMore()
            }

        }
        binding.addCheckBox.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked != noteEntity.checkMode) {
                noteEntity.checkMode = isChecked
                if (noteEntity.checkMode) {

                    var list = splitText(noteEntity.text)

                    val checkString = noteEntity.checkString
                    if (checkString == "") {
                        list?.forEach {
                            noteEntity.checkString += "0"
                        }
                    }
                    //should create new checkString
                    noteEntity.checkString = ""
                    for (i in 0 until list!!.size) {
                        noteEntity.checkString += "0"
                    }

                    showCheckMode(noteEntity, list!!, onNoteBindData)
                } else {
                    showTxtMode()
                }
                onNoteBindData.updateData(noteEntity)
            }
        }

    }

    private fun hideMore() {
        binding.deleteNote.visibility = GONE
        binding.forwardNote.visibility = GONE
    }

    private fun showMore() {
        binding.deleteNote.visibility = VISIBLE
        binding.forwardNote.visibility = VISIBLE
    }

    private fun showCheckBox() {
        binding.addCheckBox.visibility = View.VISIBLE
        binding.txtCheckBox.visibility = VISIBLE
    }

    private fun hideCheckBox() {
        binding.addCheckBox.visibility = View.GONE
        binding.txtCheckBox.visibility = GONE
    }
    private fun handleBottomCommands(onNoteBindData: OnNoteBindData){
        binding.deleteNote.setOnClickListener {
            val builder = AlertDialog.Builder(contextInstance)

            builder.setTitle(contextInstance.getString(R.string.delete_note_title))
            builder.setMessage(contextInstance.getString(R.string.delete_note_text))
            builder.setPositiveButton("Yes") { dialog, which ->
               Toast.makeText(contextInstance, contextInstance.getString(R.string.delete_successfully), Toast.LENGTH_SHORT).show()
                onNoteBindData.deleteData(noteEntity)
                activityUtil.setFragment(HomeFragment(activityUtil))
            }
            builder.setNegativeButton("No") { dialog, which ->
                dialog.dismiss()
            }
            val dialog = builder.create()
            dialog.show()
        }
        binding.forwardNote.setOnClickListener {
            val shareIntent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, noteEntity.text)
                type = "text/plain"
            }
            contextInstance.startActivity(Intent.createChooser(shareIntent, noteEntity.title))

        }

    }



}