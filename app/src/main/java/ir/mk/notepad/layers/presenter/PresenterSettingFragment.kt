package ir.mk.notepad.layers.presenter

import ir.mk.notepad.entities.NoteEntity
import ir.mk.notepad.layers.ext.BaseLifeCycle
import ir.mk.notepad.layers.ext.OnNoteBindData
import ir.mk.notepad.layers.model.ModelSettingFragment
import ir.mk.notepad.layers.view.ViewSettingFragment

class PresenterSettingFragment(
    private val view: ViewSettingFragment,
    private val model: ModelSettingFragment
) : BaseLifeCycle {
    override fun onCreate() {
        view.handleDark()
    }

}