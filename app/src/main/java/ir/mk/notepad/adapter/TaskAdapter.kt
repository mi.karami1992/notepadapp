import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ir.mk.notepad.adapter.RecyclerDiffUtils
import ir.mk.notepad.databinding.TaskBoxBinding
import ir.mk.notepad.entities.Task
import ir.mk.notepad.layers.ext.OnTaskBindData

class TaskAdapter(
    private var tasks: ArrayList<Task>,
    private val onTaskBindData: OnTaskBindData
) : RecyclerView.Adapter<TaskAdapter.TaskViewHolder>() {
    private lateinit var context: Context

    inner class TaskViewHolder(private val binding: TaskBoxBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setData(task: Task, position: Int) {
            binding.checkbox.isChecked = task.isChecked
            binding.txtSentence.setText(task.sentence)
            binding.txtSentence.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
                override fun afterTextChanged(string: Editable?) {
                    task.sentence=string.toString()
                    onTaskBindData.updateSentence(task, position)

                }
            })
            binding.checkbox.setOnCheckedChangeListener { _, isChecked ->
                task.isChecked = isChecked
                onTaskBindData.updateCheckMode(task, position)
//                var checkString = noteEntity.checkString
//                val temp = checkString
//                checkString = checkString.substring(
//                    0,
//                    position
//                ) + (if (isChecked) 1 else 0) + temp.substring(position + 1)
//                noteEntity.checkString = checkString
//                onNoteBindData.updateData(noteEntity)
            }
            binding.closeBtn.setOnClickListener {
                tasks.forEach {
                    Log.i("XXXXX",it.sentence)
                }
                Log.i("XXXXX","XXXXXXXXXXXXXXXXXXXXXXXXXX")
                onTaskBindData.deleteTask(task, position)
                tasks.forEach {
                    Log.i("XXXXX",it.sentence)
                }
                Log.i("XXXXX",tasks.size.toString())

//                tasks.removeAt(position)
                notifyItemRemoved(position)
            }
            Log.i("end", "_________________")
        }

        private fun newSentence(): String {
            var newTxt: String = ""
            tasks.forEach { newTxt = "$newTxt${it.sentence}\n" }
            return newTxt.substring(0, newTxt.length - 2)
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        this.context = parent.context
        val binding = TaskBoxBinding.inflate(LayoutInflater.from(this.context), parent, false)
        return TaskViewHolder(binding)
    }

    override fun getItemCount(): Int = tasks.size
    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.setData(tasks[position], position)

    }
    fun AppCompatEditText.updateText(text: String) {
        val focussed = hasFocus()
        if (focussed) {
            clearFocus()
        }
        setText(text)
        if (focussed) {
            requestFocus()
        }
    }

    fun addnew(task:Task) {
        notifyItemInserted(tasks.size)

    }
}