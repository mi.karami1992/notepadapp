package ir.mk.notepad.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import ir.mk.notepad.R
import ir.mk.notepad.databinding.FixTaskBoxBinding
import ir.mk.notepad.entities.Task

class FixTaskAdapter(private val context:Activity,
                     private val data: ArrayList<Task>
):ArrayAdapter<Task>(context,R.layout.fix_task_box,data) {
    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val binding = FixTaskBoxBinding.inflate(context.layoutInflater)
        binding.txtFixSentence.text=data[position].sentence
        binding.fixCheckBoxx.isChecked=data[position].isChecked
        return binding.root
    }
}