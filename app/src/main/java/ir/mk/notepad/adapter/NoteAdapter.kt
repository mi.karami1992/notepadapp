package ir.mk.notepad.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ir.mk.notepad.databinding.NoteItemBinding
import ir.mk.notepad.entities.NoteEntity
import ir.mk.notepad.entities.Task
import ir.mk.notepad.layers.ext.ActivityUtils
import ir.mk.notepad.layers.ext.OnNoteBindData
import ir.mk.notepad.layers.ui.fragments.NoteFragment

class NoteAdapter(
    private val activity: Activity,
    private val notes: ArrayList<NoteEntity>,
    private val onBindData: OnNoteBindData,
    private val activityUtils: ActivityUtils
) :
    RecyclerView.Adapter<NoteAdapter.NoteViewHolder>() {
    private lateinit var context: Context

    inner class NoteViewHolder(private val binding: NoteItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setData(note: NoteEntity, position: Int) {
            binding.noteTitle.text = note.title
            if (!note.checkMode) {
                binding.noteTxt.text = note.text
                binding.noteTxt.visibility = View.VISIBLE
                binding.noteItemListView.visibility = View.GONE
            } else {
                binding.noteTxt.visibility = View.GONE
                binding.noteItemListView.visibility = View.VISIBLE

                // Setting the adapter

                var list = splitText(note.text)!!
                var content: ArrayList<Task> = arrayListOf()
                for (i in 0 until 3) {
                    if (i in list.indices) {
                        val checkChar = note.checkString.get(i)
                        content.add(Task(list.get(i), checkChar != '0'))
                    }
                }
                val adapter = FixTaskAdapter(activity, content)
                binding.noteItemListView.adapter = adapter


            }
            binding.deleteBtn.setOnClickListener {
                onBindData.deleteData(note)
            }
            binding.root.setOnClickListener {
                navigate(note)
            }
            binding.noteItemListView.setOnItemClickListener { _, _, _, _ ->
                navigate(note)
            }
        }

        fun navigate(note: NoteEntity) {
//            val intent = Intent(context, AddNoteActivity::class.java)
//            intent.putExtra("id", note.id)
//            context.startActivity(intent)
            activityUtils.setFragment(NoteFragment(note.id,activityUtils))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        this.context = parent.context
        val binding = NoteItemBinding.inflate(LayoutInflater.from(this.context), parent, false)
        return NoteViewHolder(binding)
    }

    override fun getItemCount(): Int = notes.size

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.setData(notes[position], position)
    }

    fun dataUpdate(newList: ArrayList<NoteEntity>) {
        val diffCallback = RecyclerDiffUtils(notes, newList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        notes.clear()
        notes.addAll(newList)
        diffResult.dispatchUpdatesTo(this)
    }

    private fun splitText(text: String): List<String>? {
        var list = text?.split("\n")
        if (list?.last() == "")
            list = list?.dropLast(1)
        return list

    }
}