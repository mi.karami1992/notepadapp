package ir.mk.notepad.entities

data class Task(
    var sentence:String,
    var isChecked:Boolean
)
