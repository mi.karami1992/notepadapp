package ir.mk.notepad.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import ir.mk.notepad.db.DBHandler
import java.util.Date

@Entity(tableName = DBHandler.NOTE_TABLE)
data class NoteEntity(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    @ColumnInfo var title: String,
    @ColumnInfo var text: String,
    @ColumnInfo var checkString:String,
    @ColumnInfo var checkMode:Boolean = false,
    @ColumnInfo var createdAt: Long ,
    @ColumnInfo var updatedAt: Long
) {
}